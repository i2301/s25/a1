

    db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$count: "fruitsOnSale" }
    ])


    db.fruits.aggregate([
        {$match: {"stock": {$gt: 20}}},
        {$count: "fruitsOnSale" }
    ])




    db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: {_id: null, ave_price: {$avg: "$price"}}}
    ])



    db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: {_id: null, max_price: {$max: "$price"}}}
    ])


    db.fruits.aggregate([
        {$match: {"onSale": true}},
        {$group: {_id: null, min_price: {$min: "$price"}}}
    ])
